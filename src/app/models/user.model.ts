export interface IUser {
  about: string;
  author?: boolean;
  avatar?: string;
  city: string;
  country: string;
  email: string;
  id: number;
  phone_number: string;
  rating: string;
  surname?: string;
  username: string;
  year?: number;
  subscribers_amount?: number;
  subscriptions_amount?: number;
  video_amount?: number;
}
