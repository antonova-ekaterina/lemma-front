export interface IBroadcast {
  id: number;
  name: string;
  time_start: string;
  free: boolean;
  status?: number;
  chat_mode?: boolean;
  created_at?: string;
  description?: string;
  duration?: number;
  freetime?: number;
  preview?: string;
  price?: string;
}
