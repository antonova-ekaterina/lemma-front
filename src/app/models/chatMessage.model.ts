export interface IChatMessage {
  created_at: string;
  id: number;
  pin: boolean;
  stream: number;
  text: string;
  user_id: number;
  user_name: string;
}
