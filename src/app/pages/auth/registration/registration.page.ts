import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: 'registration.page.html',
  styleUrls: ['registration.page.scss']
})
export class RegistrationPageComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  phone: string = null;

  mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private userService: UserService,
              private router: Router) {
  }


  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      country: ['RU', Validators.required],
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  registration() {
    const newUserInfo = {
      country: this.firstFormGroup.getRawValue().country,
      username: this.firstFormGroup.getRawValue().name,
      email: this.firstFormGroup.getRawValue().email,
      phone_number: '8' + this.firstFormGroup.getRawValue().phone.replace(/\D+/g, '')
    };
    this.authService.registration(newUserInfo)
      .subscribe(res => {
        if (!res || !res.success) {
          if (res && res.errors.description.indexOf('phone_number: [\'This field must be unique.\']') >= 0) {
            return alert('Пользователь с таким номером уже зарегистрирован');
          }
          if (res && res.errors.description.indexOf('username: [\'This field must be unique.\']') >= 0) {
            return alert('Пользователь с таким именем уже зарегистрирован');
          }
          if (res && res.errors.description.indexOf('email: [\'This field must be unique.\']') >= 0) {
            return alert('Пользователь с таким email уже зарегистрирован');
          }
          return alert('Неизвестная ошибка, попробуйте зайти позже');
        }
        this.phone = res.data.user.phone_number;
      });
  }

  verifyCode(phoneNumber, SMSCode) {

    this.authService.verifyCode(phoneNumber, SMSCode)
      .subscribe(res => {
        if (!res || !res.success) {
          return alert('Код введен неверно');
        }

        this.userService.setUser(res.data.user);
        this.authService.setToken(res.data.token);
        this.router.navigate(['/user']);
      });
  }

}
