import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-log-in',
  templateUrl: 'log-in.page.html',
  styleUrls: ['log-in.page.scss']
})
export class LogInPageComponent implements OnInit {

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

  constructor(private _formBuilder: FormBuilder,
              private _authService: AuthService,
              private _userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  logIn(phoneNumber) {

    const phone = '8' + phoneNumber.replace(/\D+/g, '');

    this._authService.logIn(phone)
      .subscribe(
        res => {
          if (!res || !res.success) {
            return alert('Номер введен неверно');
          }
        });
  }

  verifyCode(phoneNumber, SMSCode) {

    const phone = '8' + phoneNumber.replace(/\D+/g, '');

    this._authService.verifyCode(phone, SMSCode)
      .subscribe(res => {
        if (!res || !res.success) {
          return alert('Код введен неверно');
        }

        this._userService.setUser(res.data.user);
        this._authService.setToken(res.data.token);
        this.router.navigate(['/user']);
      });
  }

}
