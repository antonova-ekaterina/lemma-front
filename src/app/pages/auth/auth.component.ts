import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/user.service';

@Component({
  templateUrl: 'auth.component.html'
})
export class AuthComponent implements OnInit {
  constructor(private userService: UserService,
              private authService: AuthService,
              private router: Router) {

  }

  ngOnInit() {
    this.userService.getUser()
      .subscribe(res => {
        if (!res || !res.success) {
          return;
        }
        this.router.navigate(['/user']);
        this.userService.setUser(res.data.user);
      });
  }
}
