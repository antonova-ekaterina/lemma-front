import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() isAuthorized = false;
  @Input() userName = null;

  constructor(private authService: AuthService,
              private router: Router,
              private userService: UserService) {

  }

  ngOnInit() {
    const user = this.userService.getUserFromStorage();
    this.userName = user ? user.username : null;
  }

  logout() {
    this.authService.logOut();
    this.router.navigate(['/']);
  }
}
