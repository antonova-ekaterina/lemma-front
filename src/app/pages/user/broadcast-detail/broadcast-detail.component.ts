import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {BroadcastService} from '../../../services/broadcast.service';
import {IBroadcast} from '../../../models/broadcast.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-broadcast-detail',
  templateUrl: 'broadcast-detail.component.html',
  styleUrls: ['broadcast-detail.component.scss']
})
export class BroadcastDetailComponent implements OnInit {

  broadcast: IBroadcast = null;
  newBroadcastInfo: FormGroup;
  editMode = false;
  showPriseField = 0;
  mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];

  constructor(private route: ActivatedRoute,
              private broadcastService: BroadcastService,
              private fb: FormBuilder,
              private router: Router) {

  }

  ngOnInit() {
    this.route.paramMap
      .pipe(map((params: ParamMap) => params.get('broadcastId')))
      .subscribe(broadcastId => this.getBroadcastDetail(broadcastId));
  }

  getBroadcastDetail(broadcastId) {
    this.broadcastService.getBroadcastDetail(broadcastId)
      .subscribe((broadcast: IBroadcast) => this.setBroadcastData(broadcast));
  }

  setBroadcastData(broadcast: IBroadcast) {
    this.broadcast = broadcast;
    if (!this.broadcast.free) {
      this.showPriseField = 1;
    }
    this.newBroadcastInfo = this.fb.group({
      name: [this.broadcast.name, Validators.required],
      time: [CommonService.getStringWithHourAndMinute(this.broadcast.time_start), Validators.required],
      date: [new Date(this.broadcast.time_start), Validators.required],
      free: [this.broadcast.free.toString(), Validators.required],
      duration: this.broadcast.duration,
      freetime: this.broadcast.freetime,
      preview: this.broadcast.preview,
      price: this.broadcast.price,
      description: this.broadcast.description
    });

  }

  changeBroadcastInfo() {

    const name = this.newBroadcastInfo.getRawValue().name;
    const free = this.newBroadcastInfo.getRawValue().free;
    const price = this.newBroadcastInfo.getRawValue().price;
    const date = this.newBroadcastInfo.getRawValue().date;
    const time = this.newBroadcastInfo.getRawValue().time;
    const description = this.newBroadcastInfo.getRawValue().description;

    const newBroadcastInfo = {
      id: this.broadcast.id,
      name: name,
      free: free,
      price: free ? 0 : price,
      time_start: CommonService.setHours(
        date,
        CommonService.parseHoursAndMinutes(time).hours,
        CommonService.parseHoursAndMinutes(time).minutes
      ),
      freetime: free ? 0 : 0,
      description: description
    };

    this.broadcastService.editBroadcastData(newBroadcastInfo)
      .subscribe((broadcast: IBroadcast) => {
        this.setBroadcastData(broadcast);
        alert('изменения сохранены');
        this.editMode = false;
      });
  }

  startBroadcast() {
    this.router.navigate([`user/broadcasting`, this.broadcast.id]);
  }
}
