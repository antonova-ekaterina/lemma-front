import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as red5prosdk from '../../../lib/red5pro/red5pro-sdk.min.js';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {UserService} from '../../../services/user.service';
import {IChatMessage} from '../../../models/chatMessage.model';

@Component({
  selector: 'app-broadcast',
  templateUrl: 'broadcasting.page.html',
  styleUrls: ['broadcasting.page.scss']
})
export class BroadcastPageComponent implements AfterViewInit {

  streamId: string;

  constructor(private route: ActivatedRoute) {
  }

  ngAfterViewInit() {
    this.route.paramMap.pipe(
      map((params: ParamMap) => {
        return params.get('id');
      })
    ).subscribe((id: string) => {
      this.streamId = id;
      this.createBroadcast(id);
    });
  }

  createBroadcast(id) {
    const rtcPublisher = new red5prosdk.RTCPublisher();
    const rtcSubscriber = new red5prosdk.RTCSubscriber();
    const config = {
      protocol: 'wss',
      host: 'red5.wilix.org',
      port: 8083,
      app: 'live',
      streamName: `stream${id}`,
      iceServers: window['r5proIce']
    };

    function subscribe() {
      rtcSubscriber.init(config)
        .then(function () {
          return rtcSubscriber.subscribe();
        })
        .then(function () {
          console.log('Playing!');
        })
        .catch(function (err) {
          console.log('Could not play: ' + err);
        });
    }

    rtcPublisher
      .init(config)
      .then(function () {
        // On broadcasting started, subscribe.
        rtcPublisher.on(red5prosdk.PublisherEventTypes.PUBLISH_START, subscribe);
        return rtcPublisher.publish();
      })
      .then(function () {
        console.log('Publishing!');
      })
      .catch(function (err) {
        console.error('Could not publish: ' + err);
      });
  }
}
