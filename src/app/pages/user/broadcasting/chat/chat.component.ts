import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../../../services/chat.service';
import {IChatMessage} from '../../../../models/chatMessage.model';

@Component({
  selector: 'app-chat',
  templateUrl: 'chat.component.html',
  styleUrls: ['chat.component.scss']
})
export class ChatComponent implements OnInit {
  @Input() streamId: string;
  @ViewChild('scroll') scroll: ElementRef;
  messageText: string;
  comments: IChatMessage[] = [];

  showMuteBlock = false;
  muteUserName = '';

  constructor(private chatService: ChatService) {
  }

  ngOnInit() {
    this.chatService.getAllMessage(this.streamId)
      .subscribe((messages: IChatMessage[]) => {
        this.comments = [...messages];
        this.scrollTOBottom(this.scroll.nativeElement);
      });

    this.chatService.startGettingChatMessages(this.streamId)
      .subscribe((message: IChatMessage) => {
          this.comments = [message, ...this.comments];
          this.scrollTOBottom(this.scroll.nativeElement);
        },
        err => console.log('err in get mess in socket connection = ', err),
        () => console.log('Completed!')
      );
  }

  sendMessage() {
    this.chatService.sendMessage(this.streamId, this.messageText);
    this.messageText = '';
  }

  scrollTOBottom(div) {
    setTimeout(() => {
      div.scrollTop = div.scrollHeight;
    }, 1000);
  }

  muteUser(message: IChatMessage) {
    this.showMuteBlock = true;
    this.muteUserName = message.user_name;
  }

}
