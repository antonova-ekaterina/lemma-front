import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {BroadcastService} from '../../../services/broadcast.service';
import {UserService} from '../../../services/user.service';
import {IUser} from '../../../models/user.model';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-planning-broadcast',
  templateUrl: 'planning-broadcast.component.html',
  styleUrls: ['planning-broadcast.component.scss'],
})

export class PlanningBroadcastComponent implements OnInit {

  user: IUser = null;
  newBroadcastForm: FormGroup;
  showPriseField = false;
  mask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];

  constructor(private userService: UserService,
              private router: Router,
              private formBuilder: FormBuilder,
              private broadcastService: BroadcastService) {

  }

  ngOnInit() {
    this.user = this.userService.getUserFromStorage();
    this.initForm();
  }

  initForm() {
    this.newBroadcastForm = this.formBuilder.group({
      name: ['', Validators.required],
      time: ['', Validators.required],
      free: ['true', Validators.required],
      date: ['', Validators.required],
      price: ['0', Validators.required],
    });
  }

  planningBroadcast() {
    const name = this.newBroadcastForm.getRawValue().name;
    const free = this.newBroadcastForm.getRawValue().free;
    const price = this.newBroadcastForm.getRawValue().price;
    const date = this.newBroadcastForm.getRawValue().date;
    const time = this.newBroadcastForm.getRawValue().time;

    const newBroadcast = {
      name: name,
      free: free,
      price: free ? 0 : price,
      time_start: CommonService.setHours(
        date,
        CommonService.parseHoursAndMinutes(time).hours,
        CommonService.parseHoursAndMinutes(time).minutes
      ),
      freetime: free ? 0 : 0,
    };
    this.broadcastService.createBroadcast(newBroadcast)
      .subscribe((res) => {
        if (res || res.success) {
          alert('Стрим запланирован');
          return this.router.navigate([`/`]);
        }
        alert('Произошла неизвестная ошибка');
        this.router.navigate(['/']);
      });
  }


}
