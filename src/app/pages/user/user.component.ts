import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IUser} from '../../models/user.model';
import {UserService} from '../../services/user.service';

@Component({
  templateUrl: 'user.component.html'
})
export class UserComponent implements OnInit {
  user: IUser = null;

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {

    this.userService.getUser()
      .subscribe(res => {
        if (!res || !res.success) {
          return this.router.navigate(['/']);
        }
        this.userService.setUser(res.data.user);
        this.user = res.data.user;
      });
  }
}
