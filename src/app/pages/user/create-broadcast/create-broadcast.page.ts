import {Component, OnInit} from '@angular/core';
import {IUser} from '../../../models/user.model';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BroadcastService} from '../../../services/broadcast.service';

@Component({
  selector: 'app-create-broadcast',
  templateUrl: 'create-broadcast.page.html',
  styleUrls: ['create-broadcast.page.scss']
})
export class CreateBroadcastPageComponent implements OnInit {

  user: IUser = null;
  newBroadcastForm: FormGroup;
  showPriseField = false;

  constructor(private userService: UserService,
              private router: Router,
              private formBuilder: FormBuilder,
              private broadcastService: BroadcastService) {
  }

  ngOnInit() {
    this.user = this.userService.getUserFromStorage();
    this.initForm();
  }

  initForm() {
    this.newBroadcastForm = this.formBuilder.group({
      name: ['', Validators.required],
      free: ['true', Validators.required],
      price: ['0', Validators.required],
    });
  }

  createBroadcast() {
    const broadcast = {
      name: this.newBroadcastForm.getRawValue().name,
      free: this.newBroadcastForm.getRawValue().free,
      price: this.newBroadcastForm.getRawValue().free ? 0 : this.newBroadcastForm.getRawValue().price,
      time_start: new Date(),
      freetime: this.newBroadcastForm.getRawValue().free ? 0 : 0,
    };
    this.broadcastService.createBroadcast(broadcast)
      .subscribe((res) => {
        if (!res || !res.success) {
          alert('Произошла ошибка');
          this.router.navigate(['/']);
        }
        this.router.navigate([`user/broadcasting`, res.data.stream.id]);
      });
  }

}
