import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IUser} from '../../../models/user.model';
import {UserService} from '../../../services/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile-info',
  templateUrl: 'profile-info.component.html',
  styleUrls: ['profile-info.component.scss']
})
export class ProfileInfoComponent {

  user: IUser = null;
  editMode = false;
  mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

  newProfileInfo: FormGroup;

  constructor(private userService: UserService,
              private router: Router,
              private fb: FormBuilder) {
  }

  setUserData(user: IUser) {
    this.user = user;
    this.newProfileInfo = this.fb.group({
      username: [this.user.username, Validators.required],
      phone_number: [this.user.phone_number.substr(1), Validators.required],
      email: [this.user.email, Validators.required],
      country: this.user.country,
      city: this.user.city,
      about: this.user.about
    });
  }

  changeProfileInfo() {
    const phoneNumber = this.newProfileInfo.value.phone_number && ('8' + this.newProfileInfo.value.phone_number.replace(/\D+/g, ''));

    const newUserInfo = {
      ...this.newProfileInfo.value,
      phone_number: phoneNumber
    };

    this.userService.editProfile(this.user.id, newUserInfo)
      .subscribe((res: any) => {
        this.setUserData(res.data.user);
        alert('изменения сохранены');
        this.editMode = false;
      });
  }

}
