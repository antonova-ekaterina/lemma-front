import {Component, OnChanges, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BroadcastService} from '../../../services/broadcast.service';
import {IBroadcast} from '../../../models/broadcast.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-calendar',
  templateUrl: 'calendar.component.html',
  styleUrls: ['calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  monthOptions: FormGroup;
  plannedBroadcast: IBroadcast[] = null;

  constructor(private fb: FormBuilder,
              private broadcastService: BroadcastService,
              private router: Router) {
    this.monthOptions = fb.group({
      month: (new Date).getMonth().toString(),
      year: (new Date).getFullYear().toString()
    });
  }

  ngOnInit() {
    this.requestSampleOfBroadcasts();
  }

  requestSampleOfBroadcasts() {
    const year = this.monthOptions.value.year;
    const month = +this.monthOptions.value.month + 1;

    this.broadcastService.getPlannedBroadcast(year, month)
      .subscribe((broadcasts) => {
        this.plannedBroadcast = broadcasts;
      });
  }

  showDetail(broadcastId) {
    this.router.navigate(['user', 'broadcast-detail', broadcastId])
      .catch(() => alert('Произошла ошибка, попробуйте позже'));
  }

  getDayAndMonth(stringDate: string): string {
    const date = new Date(stringDate);
    const day = date.getDate();
    const monthNumber = date.getMonth();
    let month;
    switch (monthNumber) {
      case 0:
        month = 'январь';
        break;
      case 1:
        month = 'феараль';
        break;
      case 2:
        month = 'март';
        break;
      case 3:
        month = 'апрель';
        break;
      case 4:
        month = 'май';
        break;
      case 5:
        month = 'июнь';
        break;
      case 6:
        month = 'июль';
        break;
      case 7:
        month = 'август';
        break;
      case 8:
        month = 'сентябрь';
        break;
      case 9:
        month = 'октябрь';
        break;
      case 10:
        month = 'ноябрь';
        break;
      case 11:
        month = 'декабрь';
        break;
    }
    return `${day} ${month}`;
  }

  getWeekDayName(stringDate: string): string {
    const date = new Date(stringDate);
    switch (date.getDay()) {
      case 0:
        return 'понедельник';
      case 1:
        return 'вторник';
      case 2:
        return 'среда';
      case 3:
        return 'четверг';
      case 4:
        return 'пятница';
      case 5:
        return 'суббота';
      case 6:
        return 'воскресенье';
    }
  }
}
