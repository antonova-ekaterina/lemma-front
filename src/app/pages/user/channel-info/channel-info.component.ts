import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IUser} from '../../../models/user.model';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-channel-info',
  templateUrl: 'channel-info.component.html',
  styleUrls: ['channel-info.component.scss']
})
export class ChannelInfoComponent implements OnInit {

  user: IUser = null;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.user = this.userService.getUserFromStorage();
  }

}
