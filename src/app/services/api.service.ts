import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';

const HOST = 'https://lema.wilix.org/api/v1';

@Injectable()
export class ApiService {

  httpOptions = null;

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token ${localStorage.getItem('token')}`
      })
    };
  }

  post(url: string, body = {}, errMessage: string = ''): Observable<any> {

    return this.http.post(`${HOST}${url}/`, body, this.httpOptions)
      .pipe(
        catchError(this.handleError(errMessage))
      );
  }

  get(url: string, params = null, errMessage: string = ''): Observable<any> {
    let fullURL = `${HOST}${url}/`;
    if (params) {
      fullURL = `${fullURL}?`;

      Object.keys(params).forEach(key => {
        fullURL = `${fullURL}${key}=${params[key]}&`;
      });
    }
    return this.http.get(fullURL, this.httpOptions)
      .pipe(
        catchError(this.handleError(errMessage))
      );
  }

  put(url: string, payload, errMessage: string = ''): Observable<any> {
    return this.http.put(`${HOST}${url}/`, payload, this.httpOptions)
      .pipe(
        catchError(this.handleError(errMessage))
      );
  }

  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      alert('Ошибка, попробуйте в другое время');
      console.log(`err = ${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  setToken(): void {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token ${localStorage.getItem('token')}`
      })
    };
  }

  clearToken(): void {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

}
