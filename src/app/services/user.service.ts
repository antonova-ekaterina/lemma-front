import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {IUser} from '../models/user.model';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class UserService {

  user: IUser = null;

  constructor(private apiService: ApiService,
              private router: Router) {
  }

  getUser(): Observable<any> {
    return this.apiService.get('/accounts/get_user_info', null, 'err in get user info');
  }

  getUserFromStorage(): IUser {
    if (this.user) {
      return this.user;
    }
    this.router.navigate(['/']);
    return null;
  }

  setUser(user: IUser) {
    this.user = user;
  }

  editProfile(id, data): Observable<IUser> {
    return this.apiService.put(`/accounts/${id}`, data);
  }

}
