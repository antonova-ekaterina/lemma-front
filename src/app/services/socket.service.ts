import {Injectable} from '@angular/core';
import {WebSocketSubject} from 'rxjs/webSocket';

@Injectable()
export class SocketService {

  initConnection(socketURL: string): WebSocketSubject<any> {
    return new WebSocketSubject(socketURL);
  }

  sendMessage(socketConnection: WebSocketSubject<any>, message): void {
    socketConnection.next(message);
  }
}
