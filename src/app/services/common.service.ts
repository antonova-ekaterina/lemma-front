import {Injectable} from '@angular/core';

@Injectable()
export class CommonService {

  static parseHoursAndMinutes(time: string = '00:00') {
    const hours = time.substring(0, time.indexOf(':'));
    const minutes = time.substring(time.indexOf(':') + 1);
    return {
      hours: +hours,
      minutes: +minutes
    };
  }

  static setHours(date: Date, hours: number = 0, minutes: number = 0): Date {
    const newDate = date.setHours(hours, minutes);
    return new Date(newDate);
  }


  static getStringWithHourAndMinute(stringDate: string): string {
    const date = new Date(stringDate);
    const hours = date.getHours();
    const strHours = hours < 10 ? '0' + date.getHours() : date.getHours().toString();
    const minutes = date.getMinutes();
    const strMinutes = minutes < 10 ? '0' + minutes : minutes.toString();

    return strHours + strMinutes;
  }
}
