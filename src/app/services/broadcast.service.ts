import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {IBroadcast} from '../models/broadcast.model';
import {of} from 'rxjs/internal/observable/of';

@Injectable()
export class BroadcastService {

  constructor(private apiService: ApiService) {
  }

  createBroadcast(broadcast): Observable<any> {
    return this.apiService.post('/streams', broadcast, 'err in create broadcasting');
  }

  getPlannedBroadcast(year = null, month = null, param: any = {}): Observable<IBroadcast[]> {
    if (year) {
      if (month) {
        param.time_start__month = month;
      }
      param.time_start__year = year;
    }
    return this.apiService.get('/plan', param)
      .pipe(
        map(res => res.data.streams),
        catchError(() => of([]))
      );
  }

  getBroadcastDetail(broadcastId): Observable<IBroadcast> {
    return this.apiService.get(`/plan/${broadcastId}`)
      .pipe(
        map(res => res.data.stream),
        catchError(() => of({}))
      );
  }

  editBroadcastData(newBroadcastData): Observable<any> {
    return this.apiService.put(`/streams/${newBroadcastData.id}`, newBroadcastData)
      .pipe(
        map(res => res.data.stream),
        catchError(() => of({})));
  }
}
