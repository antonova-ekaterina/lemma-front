import {Injectable} from '@angular/core';
import {WebSocketSubject} from 'rxjs/webSocket';
import {Observable} from 'rxjs/internal/Observable';
import {ApiService} from './api.service';
import {AuthService} from './auth.service';
import {SocketService} from './socket.service';
import {map} from 'rxjs/operators';
import {IChatMessage} from '../models/chatMessage.model';

const CHAT_URL = 'ws://wilix.org:8001/ws/chat/';
// const CHAT_URL = 'wss://lema.wilix.org/ws/chat/';

@Injectable()
export class ChatService {
  private socketConnection$: WebSocketSubject<{ data: { message: IChatMessage } }>;

  constructor(private authService: AuthService,
              private apiService: ApiService,
              private socketService: SocketService) {
  }

  startGettingChatMessages(streamId: string): Observable<IChatMessage> {
    this.socketConnection$ = this.socketService.initConnection(`${CHAT_URL}${streamId}/`);
    return this.socketConnection$.pipe(map(res => res.data.message));
  }

  sendMessage(streamId: string, messageText: string): void {
    const message = {
      type: 'chat_message',
      token: this.authService.getToken(),
      message: {
        stream_id: streamId,
        text: messageText
      }
    };
    this.socketService.sendMessage(this.socketConnection$, message);
  }

  getAllMessage(streamId: string): Observable<IChatMessage[]> {
    return this.apiService.get(`/messages`, {stream: streamId})
      .pipe(map(res => res.data.messages));
  }

}
