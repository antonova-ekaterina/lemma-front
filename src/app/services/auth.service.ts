import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';

@Injectable()
export class AuthService {

  constructor(private apiService: ApiService) {
  }

  logIn(phoneNumber): Observable<any> {
    this.apiService.clearToken();
    return this.apiService.post('/accounts/login', {'phone_number': phoneNumber}, 'logIn error');
  }

  verifyCode(phoneNumber: string, SMSCode: string): Observable<any> {
    return this.apiService.post('/accounts/phone_verification', {
      'phone_number': phoneNumber,
      'code': SMSCode
    }, 'verify error');
  }

  setToken(token: string): void {
    localStorage.setItem('token', token);
    this.apiService.setToken();
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  registration(newUserInfo): Observable<any> {
    this.apiService.clearToken();
    return this.apiService.post('/accounts', newUserInfo, 'err in registration');
  }

  isAuthorised(): Observable<any> {
    return this.apiService.get('/streams', 'err in getAll Stream');
  }

  logOut() {
    localStorage.clear();
  }
}
