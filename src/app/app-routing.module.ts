import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LogInPageComponent} from './pages/auth/log-in/log-in.page';
import {StartPageComponent} from './pages/auth/start/start.page';
import {RegistrationPageComponent} from './pages/auth/registration/registration.page';
import {CreateBroadcastPageComponent} from './pages/user/create-broadcast/create-broadcast.page';
import {BroadcastPageComponent} from './pages/user/broadcasting/broadcasting.page';
import {PlanningBroadcastComponent} from './pages/user/planning-broadcast/planning-broadcast.component';
import {AuthComponent} from './pages/auth/auth.component';
import {UserComponent} from './pages/user/user.component';
import {ProfileInfoComponent} from './pages/user/profile-info/profile-info.component';
import {ChannelInfoComponent} from './pages/user/channel-info/channel-info.component';
import {CalendarComponent} from './pages/user/calendar/calendar.component';
import {BroadcastDetailComponent} from './pages/user/broadcast-detail/broadcast-detail.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'auth/start'
  },
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      {path: 'start', component: StartPageComponent},
      {path: 'login', component: LogInPageComponent},
      {path: 'registration', component: RegistrationPageComponent},
      {path: '**', redirectTo: ''}
    ]
  },
  {
    path: 'user',
    pathMatch: 'full',
    redirectTo: 'user/calendar'
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {path: 'calendar', component: CalendarComponent},
      {path: 'profile', component: ProfileInfoComponent},
      {path: 'channel', component: ChannelInfoComponent},
      {path: 'create-broadcasting', component: CreateBroadcastPageComponent},
      {path: 'planning-broadcast', component: PlanningBroadcastComponent},
      {path: 'broadcasting/:id', component: BroadcastPageComponent},
      {path: 'broadcast-detail/:broadcastId', component: BroadcastDetailComponent},
      {path: '**', redirectTo: ''}
    ]
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}

