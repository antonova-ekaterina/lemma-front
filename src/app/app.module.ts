import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

// ToDo move to own material module
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {AppRoutingModule} from './app-routing.module';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule, MatNativeDateModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StartPageComponent} from './pages/auth/start/start.page';
import {LogInPageComponent} from './pages/auth/log-in/log-in.page';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './services/auth.service';
import {ApiService} from './services/api.service';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from './services/user.service';
import {TextMaskModule} from 'angular2-text-mask';
import {RegistrationPageComponent} from './pages/auth/registration/registration.page';
import {CreateBroadcastPageComponent} from './pages/user/create-broadcast/create-broadcast.page';
import {BroadcastService} from './services/broadcast.service';
import {BroadcastPageComponent} from './pages/user/broadcasting/broadcasting.page';
import {ChatComponent} from './pages/user/broadcasting/chat/chat.component';
import {MatSelectModule} from '@angular/material/select';
import {PlanningBroadcastComponent} from './pages/user/planning-broadcast/planning-broadcast.component';
import {SocketService} from './services/socket.service';
import {ChatService} from './services/chat.service';
import {CalendarComponent} from './pages/user/calendar/calendar.component';
import {HeaderComponent} from './pages/header/header.component';
import {AuthComponent} from './pages/auth/auth.component';
import {UserComponent} from './pages/user/user.component';
import {ProfileInfoComponent} from './pages/user/profile-info/profile-info.component';
import {ChannelInfoComponent} from './pages/user/channel-info/channel-info.component';
import {BroadcastDetailComponent} from './pages/user/broadcast-detail/broadcast-detail.component';
import {CommonService} from './services/common.service';

@NgModule({
  declarations: [
    AppComponent,
    LogInPageComponent,
    StartPageComponent,
    RegistrationPageComponent,
    CreateBroadcastPageComponent,
    BroadcastPageComponent,
    ChatComponent,
    PlanningBroadcastComponent,
    CalendarComponent,
    HeaderComponent,
    AuthComponent,
    UserComponent,
    ProfileInfoComponent,
    ChannelInfoComponent,
    BroadcastDetailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
    TextMaskModule,
    MatButtonModule,
    MatToolbarModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatExpansionModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  providers: [
    ApiService,
    AuthService,
    UserService,
    BroadcastService,
    MatDatepickerModule,
    SocketService,
    ChatService,
    CommonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
